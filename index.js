const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");

// cambiar por url
const url = "mongodb://localhost:27017";
const dbName = "ropas";

(async function () {
  const client = new MongoClient(url, { useUnifiedTopology: true });

  try {
    await client.connect();
    console.log("Connected correctly to server");

    const db = client.db(dbName);

    // Insert multiple documents
    // no importa que metas puede variar
    // lo mejor acepta json
    await db.collection("pedidos").insertMany([
      {
        usuario: "aranajhonny",
        edad: 25,
        ropa: ["pantalon", "camisa"],
        tallas: { arriba: "M", abajo: 35.5 },
      },
      {
        usuario: "ojpr15",
        edad: 26,
        ropa: ["pantalon", "camisa"],
        tallas: { arriba: "L", abajo: 34 },
        zapato: 32,
      },
    ]);

  } catch (err) {
    console.log(err.stack);
  }

  // Close connection
  client.close();
})();
